import React, { createContext, useState, useContext } from "react";
import isEmpty from "lodash/isEmpty";
import BookingApi from "../lib/booking";

export const BookingContext = createContext();

export function useBookingContext() {
  return useContext(BookingContext);
}

const BookingProvider = ({ children }) => {
  const [addBookingStatus, setAddBookingStatus] = useState('');

  const addBooking = async ({
    customerId,
    timeSlotId,
    carRegisId,
    bookingDate,
  }) => {
    if (
      isEmpty(customerId.toString()) ||
      isEmpty(timeSlotId) ||
      isEmpty(carRegisId) ||
      isEmpty(bookingDate)
    ) {
      return;
    }

    const response = await BookingApi.addBooking({
      customerId: parseInt(customerId),
      timeSlotId: parseInt(timeSlotId),
      carRegisId,
      bookingDate,
    });

    if (response?.status === 'success') {
      setAddBookingStatus(response?.status)
    }
  };

  const value = {
    addBookingStatus,
    BookingAction: {
      addBooking,
      setAddBookingStatus,
    },
  };
  return (
    <BookingContext.Provider value={value}>{children}</BookingContext.Provider>
  );
};

export default BookingProvider;
