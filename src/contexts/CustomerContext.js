import React, { createContext, useEffect, useState, useContext } from "react";
import isEmpty from "lodash/isEmpty";
import CustomerApi from "../lib/customer";

export const CustomerContext = createContext();

export function useCustomerContext() {
  return useContext(CustomerContext);
}

const CustomerProvider = ({ children }) => {
  const [suggestionCustomer, setSuggestionCustomer] = useState({});

  const featSuggestionCustomer = async ({ search }) => {
    if (isEmpty(search)) {
      setSuggestionCustomer({});
      return;
    }

    const customer = await CustomerApi.getSearchCustomer(search);

    if (!isEmpty(customer)) {
      setSuggestionCustomer(customer);
    }
  };

  const value = {
    suggestionCustomer,
    CustomerAction: {
      featSuggestionCustomer,
    },
  };
  return (
    <CustomerContext.Provider value={value}>
      {children}
    </CustomerContext.Provider>
  );
};

export default CustomerProvider;
