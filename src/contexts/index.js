export {
  default as TimeSlotProvider,
  useTimeSlotContext,
  TimeSlotContext,
} from "./TimeSlotContext";

export {
  default as CustomerProvider,
  useCustomerContext,
  CustomerContext,
} from "./CustomerContext";

export {
  default as BookingProvider,
  useBookingContext,
  BookingContext,
} from "./BookingContext";
