import React, { createContext, useEffect, useState, useContext } from "react";
import isEmpty from "lodash/isEmpty";
import find from "lodash/find";
import filter from "lodash/filter";
import size from "lodash/size";
import TimeslotApi from "../lib/timeslot";
import BookingApi from "../lib/booking";

export const TimeSlotContext = createContext();

export function useTimeSlotContext() {
  return useContext(TimeSlotContext);
}

const TimeSlotProvider = ({ children }) => {
  const [timeslot, setTimeslot] = useState({});

  const fetchTimeslot = async ({ date }) => {
    const dataBooking = await BookingApi.getBooking(date);
    let dataTimeslot = await TimeslotApi.getTimeslot();

    if (!isEmpty(dataBooking)) {
      dataTimeslot = filter(dataTimeslot, (slot) => {
        const findBooking = find(
          dataBooking,
          (booking) => booking?.time_slot_id === slot?.id
        );

        return isEmpty(findBooking);
      });
    }

    if (size(dataTimeslot)) {
      setTimeslot(dataTimeslot);
    }
  };

  const value = {
    timeslot,
    TimeSlotAction: {
      fetchTimeslot,
    },
  };
  return (
    <TimeSlotContext.Provider value={value}>
      {children}
    </TimeSlotContext.Provider>
  );
};

export default TimeSlotProvider;
