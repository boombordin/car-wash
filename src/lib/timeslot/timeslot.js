import axios from "axios";

const baseUrl = process.env.baseApiUrl;

const getTimeslot = () => {
  const params = {
    url: `${baseUrl}/timeslot`,
    method: "GET",
  };

  return axios(params)
    .then((response) => response.data)
    .catch(() => {});
};

export default {
  getTimeslot,
};
