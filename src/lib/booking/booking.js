import axios from "axios";

const baseUrl = process.env.baseApiUrl;

const getBooking = (date) => {
  const params = {
    url: `${baseUrl}/booking`,
    method: "GET",
    params: {
      date,
    },
  };

  return axios(params)
    .then((response) => response.data)
    .catch(() => {});
};

const addBooking = ({ customerId, timeSlotId, carRegisId, bookingDate }) => {
  const params = {
    url: `${baseUrl}/addBooking`,
    method: "POST",
    data: {
      customerId,
      timeSlotId,
      carRegisId,
      bookingDate,
    },
  };

  return axios(params)
    .then((response) => response.data)
    .catch(() => {
      return null;
    });
};

export default {
  getBooking,
  addBooking,
};
