import axios from "axios";

const baseUrl = process.env.baseApiUrl;

const getSearchCustomer = (search) => {
  const params = {
    url: `${baseUrl}/search/customer`,
    method: "GET",
    params: {
      search,
    },
  };

  return axios(params)
    .then((response) => response.data)
    .catch(() => {});
};

export default {
  getSearchCustomer,
};
