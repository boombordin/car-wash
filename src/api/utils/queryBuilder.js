import map from 'lodash/map';
import values from 'lodash/values';
import includes from 'lodash/includes';
import size from 'lodash/size';
import helper from './helper';

export const sortDirections = {
  ASCENDING: 'ASC',
  DESCENDING: 'DESC',
};

export const fieldConditions = {
  LESS_THAN: 'lt',
  LESS_THAN_EQUAL: 'lteq',
  GREATER_THAN: 'gt',
  GREATER_THAN_EQUAL: 'gteq',
  EQUAL: 'eq',
  NOT_EQUAL: 'neq',
  LIKE: 'like',
  FROM: 'from', // constrain: should be used with to
  TO: 'to', // constrain: should be used with from
};

class QUERYBUILDER {
  constructor() {
    this.fields = [];
    this.fieldsIndex = 0;
    this.sort_orders = [];
  }
  field(name, value, condition = fieldConditions.EQUAL) {
    if (!name || !value) {
      throw new Error('Parameter missing');
    }
    const fieldConditionValues = values(fieldConditions);
    if (!includes(fieldConditionValues, condition)) {
      // This condition is invalid
      throw new Error(
        `${condition} condition is not supported. Use one of ${fieldConditionValues.join(
          ',',
        )}`,
      );
    }

    const field = {
      name,
      value,
      condition,
    };

    this.fields = [...this.fields, field];
    this.fieldsIndex = size(this.fields);

    return this;
  }

  sort(field, direction = sortDirections.ASCENDING) {
    if (!field) {
      throw new Error('Field name missing');
    }
    const sortDirectionValues = values(sortDirections);

    if (!includes(sortDirectionValues, direction)) {
      // This direction is invalid
      throw new Error(
        `${direction} is not a valid direction. Valid directions are ${sortDirectionValues.join(
          ',',
        )}`,
      );
    }
    const sortObject = {
      field,
      direction,
    };

    this.sort_orders = [...this.sort_orders, sortObject];
    return this;
  }

  build() {
    const fieldQuery = map(this.fields, (field, index) => {
      let condition = '=';
      if (field?.condition === fieldConditions.EQUAL) {
        condition = '=';
      } else if (field?.condition === fieldConditions.NOT_EQUAL) {
        condition = '!=';
      } else if (field?.condition === fieldConditions.LIKE) {
        condition = ` LIKE `;
      } else if (field?.condition === fieldConditions.LESS_THAN) {
        condition = ` < `;
      } else if (field?.condition === fieldConditions.LESS_THAN_EQUAL) {
        condition = ` <= `;
      } else if (field?.condition === fieldConditions.GREATER_THAN) {
        condition = ` > `;
      } else if (field?.condition === fieldConditions.GREATER_THAN_EQUAL) {
        condition = ` >= `;
      }

      if (typeof field?.value == 'string') {
        let values = field?.value?.split(',');
        values = helper.clearEmpties(values);

        if (values?.length > 0) {
          values = map(values, value => {
            if (value === '') {
              return;
            }

            if (field?.condition === 'like') {
              value = `%${value}%`;
            }

            return `${field?.name}${condition}'${value}'`;
          });

          const fieldValue = [...values];

          return fieldValue?.join(' OR ');
        }
      }

      if (field?.condition === 'like') {
        value = `%${value}%`;
      }

      return `${field?.name}${condition}'${field?.value}'`;
    });

    const sortCriterias = map(this.sort_orders, (sortOrder, index) => {
      return ` ORDER BY ${sortOrder.field} ${sortOrder.direction}`;
    });

    const criterias = [...fieldQuery];
    return `${criterias?.join(' AND ')} ${sortCriterias}`;
  }
}

export default QUERYBUILDER;
