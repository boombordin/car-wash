function getOffset(currentPage = 1, listPerPage) {
  return (currentPage - 1) * [listPerPage];
}

function emptyOrRows(rows) {
  if (!rows) {
    return [];
  }
  return rows;
}

function clearEmpties(o) {
  const newObject = [];
  for (const k in o) {
    if (typeof o[k] == 'string' && o[k] !== '') {
      newObject.push(o[k]);
    }
  }

  return newObject;
}

export default {
  getOffset,
  emptyOrRows,
  clearEmpties,
};
