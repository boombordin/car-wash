import isEmpty from 'lodash/isEmpty';
import serviceMysql from './dbMysqlService';

export default (req, res, next) => {
  req.service = {
    get: ({ table, field, query, params }) => {
      let fieldTable = '*';
      if (field?.length > 0) {
        fieldTable = field?.toString();
      }

      let condition = query;
      if (!isEmpty(query)) {
        condition = `WHERE ${query}`;
      }

      const sql = `SELECT ${fieldTable} FROM ${table} ${condition}`;

      return serviceMysql.query({
        sql,
        params,
      });
    },
    post: ({ table, sql, params }) => {
      return serviceMysql.query({
        sql,
        params,
      });
    },
    // put: data => {
    //   return service({
    //     method: 'put',
    //     data,
    //   });
    // },
    // delete: data => {
    //   return service({
    //     method: 'delete',
    //     data,
    //   });
    // },
  };

  next();
};
