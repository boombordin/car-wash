import mysql from 'mysql2/promise';
import helper from './utils/helper';
import config from './dbConfig';

async function query({ sql, params }) {
  const connection = await mysql.createConnection(config.configMYSQL.db);
  let [results] = await connection.execute(sql, params);

  results = helper.emptyOrRows(results);
  return results;
}

export default {
  query,
};
