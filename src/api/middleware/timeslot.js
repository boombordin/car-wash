import QUERYBUILDER, {
  fieldConditions as Conditions,
  sortDirections,
} from '../utils/queryBuilder';

const getTimeSlot = async (req, res) => {
  const queryBuilder = new QUERYBUILDER();
  queryBuilder.field('status', 1, Conditions?.EQUAL);
  const query = queryBuilder.build();

  const results = await req.service.get({
    table: `timeslot`,
    query,
  });

  return res.status(200).json(results);
};

export default { getTimeSlot };
