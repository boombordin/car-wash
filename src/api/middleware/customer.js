import isEmpty from "lodash/isEmpty";
import QUERYBUILDER, {
  fieldConditions as Conditions,
  sortDirections,
} from '../utils/queryBuilder';

const getCustomer = async (req, res) => {
  const { search } = req.query;

  const query = `employee_id LIKE '%${search}%' OR employee_name_th LIKE '%${search}%' OR employee_name_en LIKE '%${search}%' OR telephone LIKE '%${search}%'`

  const results = await req.service.get({
    table: `customer`,
    query,
  });

  return res.status(200).json(results);
};

export default { getCustomer };
