import isEmpty from "lodash/isEmpty";
import QUERYBUILDER, {
  fieldConditions as Conditions,
  sortDirections,
} from '../utils/queryBuilder';

const getBooking = async (req, res) => {
  const { date } = req.query;
  const queryBuilder = new QUERYBUILDER();

  queryBuilder.field('booking_date', date, Conditions?.EQUAL);
  const query = queryBuilder.build();

  const results = await req.service.get({
    table: `booking`,
    query,
  });

  return res.status(200).json(results);
};

const addBooking = async (req, res) => {
  const { customerId, timeSlotId, carRegisId, bookingDate } = req.body;

  if (
    isEmpty(customerId?.toString()) ||
    isEmpty(timeSlotId?.toString()) ||
    isEmpty(carRegisId) ||
    isEmpty(bookingDate)
  ) {
    return res.status(500).json({ status: "fail", message: "data undefined" });
  }

  const sql = `INSERT INTO booking (customer_id, time_slot_id, car_regis_id, booking_date) VALUES (${customerId}, ${timeSlotId}, '${carRegisId}', '${bookingDate}')`;

  const results = await req.service.post({
    table: `booking`,
    sql,
  });

  return res.status(200).json({ status: "success" });
};

export default { getBooking, addBooking };
