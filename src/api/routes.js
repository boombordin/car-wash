import Express from "express";
import globalController from "./middleware/global";
import timeSlotController from "./middleware/timeslot";
import bookingController from "./middleware/booking";
import customerController from "./middleware/customer";

const router = Express.Router();

// version
router.get("/get-version", globalController.getVersion);

router.get("/timeslot", timeSlotController.getTimeSlot);

router.get("/booking", bookingController.getBooking);
router.post("/addBooking", bookingController.addBooking);

router.get("/search/customer", customerController.getCustomer);

export default router;
