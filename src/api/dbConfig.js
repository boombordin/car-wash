import dotenv from 'dotenv';
dotenv.config();

const env = process.env;

const configMYSQL = {
  db: {
    host: env.DB_HOST || '127.0.0.1',
    user: env.DB_USER || 'root',
    password: env.DB_PASSWORD || '',
    database: env.DB_NAME || 'twd_booking_car_wash',
  },
};

export default { configMYSQL };
