/* eslint-disable max-len */

if (process.env.BROWSER) {
  throw new Error(
    "Do not import `config.js` from inside the client-side code."
  );
}

module.exports = {
  // Node.js app
  port: process.env.PORT || 3000,

  nodeEnv: process.env.NODE_ENV !== "production",

  env: process.env.ENV || "dev",
  // https://expressjs.com/en/guide/behind-proxies.html
  trustProxy: process.env.TRUST_PROXY || "loopback",
};
