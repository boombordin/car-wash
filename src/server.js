import path from "path";
import express from "express";
import next from "next";
import bodyParser from "body-parser";
import config from "./config";
import apiRouter from "./api/routes";
import createService from './api/createService';

const dev = config?.nodeEnv;
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  //
  // If you are using proxy from external machine, you can set TRUST_PROXY env
  // Default is to trust proxy headers only from loopback interface.
  // -----------------------------------------------------------------------------
  server.set("trust proxy", config.trustProxy);
  //
  // Register Node.js middleware
  // -----------------------------------------------------------------------------
  server.use(
    express.static(path.resolve(__dirname, "public"), {
      maxAge: 31557600000,
      dotfiles: "allow",
    })
  );

  server.use(
    bodyParser.urlencoded({
      extended: true,
      // limit: '256mb',
    })
  );

  server.use(bodyParser.json());

  server.use(createService);

  server.use("/api", apiRouter);

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  //
  // Launch the server
  // -----------------------------------------------------------------------------
  if (!module.hot) {
    const serverConfig = server.listen(config.port, () => {
      console.info(`The server is running at http://localhost:${config.port}/`);
    });
    serverConfig.timeout = 60000 * 5;
    serverConfig.keepAliveTimeout = 61 * 1000;
    serverConfig.headersTimeout = 65 * 1000;
  }
});
