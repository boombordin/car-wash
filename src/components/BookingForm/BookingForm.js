import React, { useEffect, useState } from "react";
import map from "lodash/map";
import styled from "styled-components";
import { useForm, Controller } from "react-hook-form";
import dayjs from "dayjs";
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  useTimeSlotContext,
  useCustomerContext,
  useBookingContext,
} from "../../contexts";

const BookingContainer = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 991px;
`;

const BookingButtonWrap = styled.div`
  display: flex;
  justify-content: space-between;
`;

const BookingForm = styled.form`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: rgb(247, 247, 247);
  padding: 10px;
  border: 1px solid #cccccc;
  border-radius: 10px;
  margin-bottom: 10px;
`;

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  padding: 8px 0;
`;
const Title = styled.span`
  width: 80px;
  margin-bottom: 3px;
`;
const Input = styled.input`
  line-height: 30px;
  height: 30px;
  font-size: 14px;
  font-family: Sans-Serif;
  border-radius: 4px;
  border: solid 1px #cccccc;
  background-color: #ffffff;
  -webkit-appearance: none;
  width: 100%;
  padding: 0px 10px 0 10px;
  color: #2a2a2a;
  ::placeholder {
    color: #bfbfbf;
  }
  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
  }
  [type="number"] {
    -moz-appearance: textfield;
  }

  :focus {
    outline: none;
  }
`;

const BookingDatePicker = styled(ReactDatePicker)`
  line-height: 30px;
  height: 30px;
  font-size: 14px;
  font-family: Sans-Serif;
  border-radius: 4px;
  border: solid 1px #cccccc;
  background-color: #ffffff;
  -webkit-appearance: none;
  width: 100%;
  padding: 0px 10px 0 10px;
  color: #2a2a2a;
  ::placeholder {
    color: #bfbfbf;
  }
  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
  }
  [type="number"] {
    -moz-appearance: textfield;
  }

  :focus {
    outline: none;
  }
`;

const TimeSlotContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

const TimeSlotWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  border: 1px solid #cccccc;
  border-radius: 5px;
  background-color: #ffffff;
  padding: 15px;
`;
const ColTimeSlot = styled.div`
  flex: 0 0 50%;
  display: flex;
  align-items: center;
  margin: 8px 0;
`;
const Slot = styled.input``;
const TimeSlotTile = styled.span``;

const SearchBoxContainer = styled.div`
  position: relative;
`;
const SearchSuggestionContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  position: absolute;
  z-index: 2;
  top: 30px;
  background: #fff;
  box-shadow: 0 2px 15px rgba(0, 0, 0, 0.2);
  max-height: 200px;
  overflow: scroll;
`;
const SearchSuggestionList = styled.span`
  border-bottom: 1px solid rgb(207, 207, 207);
  padding: 7px 10px;
`;

const ClearButton = styled.button`
  width: 120px;
  height: 40px;
  background: #ffffff;
  border: 1px solid #808080;
  border-radius: 4px;
  cursor: pointer;

  :focus {
    outline: none;
  }
`;

const SaveButton = styled.button`
  width: 120px;
  height: 40px;
  background: none;
  background: #3866af;
  border: 1px solid #3866af;
  border-radius: 4px;
  cursor: pointer;
  color: #ffffff;

  :focus {
    outline: none;
  }
`;

const ButtonRadioWrapper = styled.label`
  display: block;
  position: relative;
  padding-left: 25px;
  margin: 0 10px 0 0;
  cursor: pointer;
  font-size: 13px;
  font-weight: 700;
  letter-spacing: -0.4px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  -webkit-tap-highlight-color: transparent;

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    z-index: -1;
  }

  &:hover input ~ .radio-checkmark {
    background-color: #eee;
  }

  input:checked ~ .radio-checkmark {
    background-color: white;
    border: 1px solid #cdcdcd;
  }

  input:checked ~ .radio-checkmark:after {
    display: block;
  }
`;

const RadioCheckMark = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 18px;
  width: 18px;
  border-radius: 50%;
  border: 1px solid #ccc;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 50%;
  transform: translateY(-50%);
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  -webkit-tap-highlight-color: transparent;

  &:after {
    content: "";
    position: absolute;
    display: none;
  }

  &:after {
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background: #80bd00;
  }
`;

let suggestionTimeoutId = null;

function FormReservation({ props }) {
  const [showSuggestion, setShowSuggestion] = useState(false);
  const [suggestion, setSuggestion] = useState("");
  const { timeslot, TimeSlotAction } = useTimeSlotContext();
  const { suggestionCustomer, CustomerAction } = useCustomerContext();
  const { addBookingStatus, BookingAction } = useBookingContext();

  const { watch, register, control, setValue, reset } = useForm();

  const handleClickSuggestion = (suggestion) => {
    setValue("customer", suggestion?.employee_name_th);
    setValue("customer_id", suggestion?.id);
    setSuggestion(suggestion?.employee_name_th);
    setShowSuggestion(false);
  };

  const handleSaveBooking = () => {
    BookingAction.addBooking({
      customerId: watch("customer_id"),
      timeSlotId: watch("timeslot"),
      carRegisId: watch("car_regis"),
      bookingDate: dayjs(watch("booking_date")).format("YYYY-MM-DD"),
    });
  };

  const getTimeSlot = () => {
    TimeSlotAction?.fetchTimeslot({ date: dayjs().format("YYYY-MM-DD") });
  };

  useEffect(() => {
    if (addBookingStatus === "success") {
      reset();
      getTimeSlot();
      BookingAction.setAddBookingStatus("");
    }
  }, [addBookingStatus]);

  useEffect(() => {
    if (suggestion === watch("customer")) return;

    if (suggestionTimeoutId) {
      clearTimeout(suggestionTimeoutId);
    }

    suggestionTimeoutId = setTimeout(() => {
      setShowSuggestion(true);
      CustomerAction?.featSuggestionCustomer({ search: watch("customer") });

      suggestionTimeoutId = null;
    }, 500);
  }, [watch("customer")]);

  useEffect(() => {
    TimeSlotAction?.fetchTimeslot({
      date: dayjs(watch("booking_date")).format("YYYY-MM-DD"),
    });
  }, [watch("booking_date")]);

  useEffect(() => {
    if (dayjs().valueOf()) {
      getTimeSlot();
    }
  }, []);

  return (
    <BookingContainer>
      <BookingForm>
        <Row>
          <FormWrapper>
            <Title>Search</Title>
            <SearchBoxContainer>
              <Input
                placeholder="Search Employee ID/ Name / Telephone..."
                autoComplete="off"
                {...register("customer")}
              />
              <Input type="hidden" {...register("customer_id")} />
              {showSuggestion && suggestionCustomer && (
                <SearchSuggestionContainer>
                  {map(suggestionCustomer, (suggest) => (
                    <SearchSuggestionList
                      onClick={() => handleClickSuggestion(suggest)}
                    >
                      {suggest?.employee_name_th}
                    </SearchSuggestionList>
                  ))}
                </SearchSuggestionContainer>
              )}
            </SearchBoxContainer>
          </FormWrapper>
        </Row>
        <Row>
          <FormWrapper>
            <Title>Car Id</Title>
            <Input
              placeholder="Car Id..."
              autoComplete="off"
              {...register("car_regis")}
            />
          </FormWrapper>
        </Row>
        <Row>
          <FormWrapper>
            <Title>Date</Title>
            <Controller
              control={control}
              name="booking_date"
              render={({ field }) => (
                <BookingDatePicker
                  className="input booking_date"
                  placeholderText="Select date"
                  onChange={(e) => field.onChange(e)}
                  selected={field.value || dayjs().valueOf()}
                  dateFormat="dd-MM-yyyy"
                  minDate={new Date()}
                  showDisabledMonthNavigation
                />
              )}
            />
          </FormWrapper>
        </Row>
        <Row>
          <TimeSlotContainer>
            <Title>Time slot</Title>
            <TimeSlotWrap>
              {map(timeslot, (slot) => (
                <ColTimeSlot>
                  <ButtonRadioWrapper>
                    <Slot
                      {...register("timeslot")}
                      type="radio"
                      value={slot?.id}
                    />
                    <TimeSlotTile>{`${slot?.start_time} - ${slot?.end_time}`}</TimeSlotTile>
                    <RadioCheckMark className="radio-checkmark"></RadioCheckMark>
                  </ButtonRadioWrapper>
                </ColTimeSlot>
              ))}
            </TimeSlotWrap>
          </TimeSlotContainer>
        </Row>
      </BookingForm>
      <BookingButtonWrap>
        <ClearButton onClick={() => reset()}>Clear</ClearButton>
        <SaveButton onClick={() => handleSaveBooking()}>Save</SaveButton>
      </BookingButtonWrap>
    </BookingContainer>
  );
}

export default FormReservation;
