import React from "react";
import styled from "styled-components";
import Header from "./components/Header";

const LayoutContainer = styled.div`
  height: 100%;
`;

function Layout({ children }) {
  return (
    <LayoutContainer>
      <Header />
      {children}
    </LayoutContainer>
  );
}

export default Layout;
