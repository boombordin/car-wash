import React from "react";
import styled from "styled-components";

const HeaderContainer = styled.div`
  height: 80px;
  background-color: #e5e5e5;
  display: flex;
  align-items: center;
  padding: 10px 20px;
`;
const LogoImage = styled.img`
  height: 46px;
`;

function Header() {
  return (
    <HeaderContainer>
      <LogoImage src="https://www.auto1.co.th/img/logo.385d0e67.png" />
    </HeaderContainer>
  );
}

export default Header;
