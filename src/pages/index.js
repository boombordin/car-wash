import React from "react";
import styled from "styled-components";
import Layout from "../components/Layout";
import BookingForm from "../components/BookingForm";
import {
  TimeSlotProvider,
  CustomerProvider,
  BookingProvider,
} from "../contexts";

const Container = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

function Home() {
  return (
    <Layout>
      <Container>
        <h1>Booking Car Wash</h1>
        <CustomerProvider>
          <BookingProvider>
            <TimeSlotProvider>
              <BookingForm />
            </TimeSlotProvider>
          </BookingProvider>
        </CustomerProvider>
      </Container>
    </Layout>
  );
}

export default Home;
