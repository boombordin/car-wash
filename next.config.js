const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");

module.exports = withPWA({
  pwa: {
    dest: "public",
    runtimeCaching,
  },
  env: {
    baseUrl: "http://localhost:3000",
    baseApiUrl: "http://localhost:3000/api",
    MYSQL_HOST: "127.0.0.1",
    MYSQL_PORT: "3306",
    MYSQL_DATABASE: "twd_booking_car_wash",
    MYSQL_USER: "root",
    MYSQL_PASSWOR: "",
  },
  webpack: (config) => {
    config.node = {
      fs: "empty",
      net: "empty",
      tls: "empty",
    };
    return config;
  },
  webpackDevMiddleware: (config) => {
    return config;
  },
});
